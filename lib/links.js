const { Parser } = require('htmlparser2-ultron')
const url = require('url')

class LinkParser extends Parser {
  constructor (options) {
    super(options)
  }
  getLinks (html, sof = 0) {
    if (typeof html !== 'string') {
      throw new TypeError('Expected html to be type of string,' +
        ' got type of ' + typeof (html) + ' instead.')
    }

    sof = isNaN(parseInt(sof)) ? html.indexOf('<body>') : sof
    if (sof > 0) html = html.slice(sof)

    const re = /<a [\S\s]*?<\/a>/gi

    let match
    while ((match = re.exec(html)) !== null) this.write(match[0])

    return this.links
  }
  apply (parser, options) {
    parser.getLinks = this.getLinks
    parser.links = []

    const { ultron } = parser

    ultron.on('opentag', (name, attribs) => {
      if (name === 'a' && attribs.href) {
        const href = options.location
          ? url.resolve(options.location, attribs.href) : attribs.href

        const link = url.parse(href)

        link.attribs = attribs

        parser.links.push(link)
        parser.emit('link', link)

        ultron.once('text', function (text) {
          link.text = text
        })
      }
    })

    ultron.on('closetag', name => {
      if (name === 'a') ultron.remove('text')
    })
  }
}

function fetchLinks (uri, options) {
  if (typeof uri === 'string') uri = { uri }

  return new Promise(async (resolve, reject) => {
    const parser = new LinkParser(options)

    parser.on('error', reject)

    parser.performRequest(uri, (error, response, body) => {
      if (error) {
        reject(error)
        return
      }

      if (!parser.options.location) { parser.options.location = response.request.href }

      resolve(parser.getLinks(body))
    })

    await parser.requests()
  })
}

module.exports = { LinkParser, fetchLinks }
