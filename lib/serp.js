const substrings = require('common-substrings')
const { power, cartesianProduct } = require('js-combinatorics')
const { Url } = require('url')

const { create } = Object

const descMap = new WeakMap()

const regex = {
  alphaNumeric: /^[a-z0-9]+$/,
  hex: /^[a-f0-9]+$/,
  partialHex: /(?:[a-f0-9]{2})+/,
  numbers: /^\d+$/,
  partialNumbers: /\d+/
}

const types = Object.keys(regex)

const escapedSpace = /-|\+|_| |%[a-f0-9]{2}/
const trailingHex = /([^a-z0-9][g-z]?)([a-f0-9]{4,})(?:\.html|\/)?$/
const trailingID = /([^a-z0-9])([a-z0-9]{4,})(?:\.html|\/)?$/

module.exports = class SERP {
  constructor (links, term) {
    'use strict'
    this.links = []
    this.potentialResults = []

    if (!Array.isArray(links)) {
      throw new TypeError('Expected links to be an array, got ' +
        typeof (links) + ' instead.')
    }

    if (!term || typeof term !== 'string') {
      throw new TypeError('Expected term to be a non-empty string, got ' +
        typeof (term) + ' instead.')
    }

    term = this.term = term.toLowerCase()

    var i = links.length
    while (i--) {
      if (!(links[i] instanceof Url)) {
        throw new TypeError('Expected link to be instance of Url, got ' +
          typeof (links[i]) + ' instead.')
      }

      var { protocol, pathname } = links[i]

      if (!pathname) continue
      if (protocol && !/^https?:$/i.test(protocol)) continue

      pathname = pathname.toLowerCase()
      links[i].segments = pathname.split('/')

      this.links.push(links[i])

      if (pathname.indexOf(term) !== -1 && escapedSpace.test(pathname)) {
        this.potentialResults.push(links[i])
      }
    }

    this.groupLinks(term)
  }
  groupLinks (term) {
    'use strict'
    var links = this.potentialResults

    var groups = {}
    var i = links.length
    // Group links by the number of path segments
    while (i--) {
      var { length } = links[i].segments

      if (groups[length]) groups[length].push(links[i])
      else groups[length] = [links[i]]
    }

    // The number of segments of the group which contains the most links
    length = Object.keys(groups)
      .sort(function (a, b) {
        return groups[b].length - groups[a].length
      })[0]

    links = groups[length]
    groups = {}

    // Determine the path segment index of torrent info name
    // by checking each segment for name pattern and keyword
    i = links.length
    while (i--) {
      var { segments } = links[i]
      var x = segments.length
      while (x--) {
        if (escapedSpace.test(segments[x]) &&
          segments[x].indexOf(term) !== -1) {
          if (groups[x]) groups[x].push(links[i])
          else groups[x] = [links[i]]
        }
      }
    }

    // The segment index of the group which contains the most links
    x = Object.keys(groups)
      .sort(function (a, b) {
        return groups[b].length - groups[a].length
      })[0]

    this.nameIndex = isNaN(+x) ? -1 : +x
    this.unfilteredResults = groups[x] || []
  }
  getPatterns (options) {
    'use strict'
    const { unfilteredResults, term } = this
    const { nameIndex, threshold, minLength } = {
      nameIndex: this.nameIndex,
      threshold: 90,
      minLength: 1,
      ...options
    }

    if (!Array.isArray(unfilteredResults)) {
      throw new TypeError('Expected results to be an array, ' +
        'got type of ' + typeof (unfilteredResults) + ' instead.')
    }

    const allSegments = {}
    const stats = {}
    const deferred = []

    var segments, counter
    var segmentA, segmentB
    var descA, descB
    var type, value
    var isNameIndex

    function score (id, value) {
      var count = counter[x]

      if (value) {
        count = count[id] || (count[id] = create(null))
        id = value
      }

      count[id] = (count[id] | 0) + 1
    }

    function grade (id, value) {
      var target = stats[x]
      var count = counter[x]

      if (value) {
        target = target[id]
        count = count[id]

        id = value
      }
      // The percentage of the time that this pattern was found in results
      var percentage = count[id] / (unfilteredResults.length - 1) * 100

      if (target[id]) {
        target[id].push(percentage)
      } else {
        target[id] = [percentage]
        // Defer ranking until all counts have been graded
        deferred.push([x, type, target[id], value])
      }
    }

    // For each result
    var i = unfilteredResults.length
    while (i--) {
      segments = unfilteredResults[i].segments
      // Create an object to store all the counts/matches for this result
      counter = {}

      // For each segment of the current result
      var x = segments.length
      while (x--) {
        if (!(segmentA = segments[x])) continue

        // Store all segments of every result by index
        if (!allSegments[x]) allSegments[x] = [segments[x]]
        else allSegments[x].push(segments[x])

        isNameIndex = nameIndex === x
        descA = getDesc(unfilteredResults[i], x, isNameIndex, term)

        // Create an object to store the counts/matches at this index
        if (!counter[x]) counter[x] = {}

        // Compare to all other results segments at same index
        var j = unfilteredResults.length
        while (j--) {
          if (i === j || !(segmentB = unfilteredResults[j].segments[x])) {
            continue
          }

          descB = getDesc(unfilteredResults[j], x, isNameIndex, term)

          if (isNameIndex) {
            if (descA.trailingHex && descB.trailingHex) {
              score('trailingHex')

              if (descA.trailingHex[1] &&
                descA.trailingHex[1] === descB.trailingHex[1]) {
                score('preHexID', descA.trailingHex[1])
              }

              segmentA = segmentA.slice(0, -descA.trailingHex[0].length)
              segmentB = segmentB.slice(0, -descB.trailingHex[0].length)

              value = getCommonSuffix(segmentA, segmentB)
              if (value && value.indexOf(term) === -1) {
                score('nameSuffix', value)
              }
            }

            if (descA.trailingID && descB.trailingID &&
              descA.trailingID[1] === descB.trailingID[1] &&
              descA.trailingID[2] !== descB.trailingID[2] &&
              descA.trailingID[2].length === descB.trailingID[2].length) {
              score('trailingID', descA.trailingID[2].length)
            }

            if (segments.length === unfilteredResults[j].segments.length) {
              score('length', segments.length)
            }

            if (descA.normalized && descB.normalized) score('normalized')

            // The remaining checks don't currently make sense for this segment
            score('escapedSpace')
            continue
          }

          if (segmentA === segmentB) score('exactPath', segmentA)
          else {
            // Remove any part of the prefix/suffix that contains digits
            value = getCommonPrefix(segmentA, segmentB).replace(/\d.*$/, '')
            if (value) {
              score('pathPrefix', value)
            }

            value = getCommonSuffix(segmentA, segmentB).replace(/^.*\d/, '')
            if (value) {
              score('pathSuffix', value)
            }
          }

          var k = types.length
          while (k--) {
            if (descA[types[k]] && descB[types[k]]) score(types[k])
          }
        }
      }

      // Grade all the counts for this result
      // For every segment index that was matched for this result
      for (x of Object.keys(counter)) {
        if (!stats[x]) stats[x] = {}

        // For every type of match counted at this segment index
        for (type of Object.keys(counter[x])) {
          if (typeof counter[x][type] !== 'number') {
            if (!stats[x][type]) stats[x][type] = create(null)
            // For every value nested under type, grade the counts of value
            for (value of Object.keys(counter[x][type])) grade(type, value)
          } else {
            grade(type)
          }
        }
      }
    }

    const patterns = {}

    deferred.forEach(function ([x, type, scores, value]) {
      // This will only be called once for each potential match
      var rank
      var i = scores.length
      var sum = 0

      while (i--) sum += scores[i]
      rank = sum / scores.length

      if (rank >= threshold) {
        if (!patterns[x]) patterns[x] = []

        if (value) {
          patterns[x].push({ index: x, type, value, rank })
        } else {
          patterns[x].push({ index: x, type, rank })
        }
      }
    })

    options = {
      minLength,
      minOccurrence: Math.ceil(unfilteredResults.length * (threshold / 100))
    }

    for (x of Object.keys(allSegments)) {
      if (+x === nameIndex) continue

      substrings(allSegments[x], options)
        .forEach(({ name, source }) => {
          if (!patterns[x]) patterns[x] = []

          patterns[x].push({
            index: x,
            type: 'substring',
            value: name,
            rank: source.length / unfilteredResults.length * 100
          })
        })
    }

    return patterns
  }
  getUnigueCombinations (patterns, options) {
    'use strict'
    const { links, unfilteredResults } = this
    const { threshold } = { threshold: 70, ...options }

    const indexes = power(Object.keys(patterns))
    const combinations = []

    var combo
    while ((combo = indexes.next())) {
      if (combo.length < 1) continue

      var patternArr

      var cp = cartesianProduct(...combo.map(x => patterns[x]))

      while (true) {
        patternArr = cp.next()
        if (!patternArr) break

        var inclusive = 0
        var exclusive = 0

        var i = links.length
        while (i--) {
          if (test(links[i], patternArr)) {
            if (unfilteredResults.indexOf(links[i]) !== -1) inclusive++
          } else {
            if (unfilteredResults.indexOf(links[i]) === -1) exclusive++
          }
        }

        inclusive = inclusive / unfilteredResults.length
        exclusive = exclusive / (links.length - unfilteredResults.length)

        if (inclusive * 100 < threshold || exclusive * 100 < threshold) {
          continue
        }

        var rank = Math.round(inclusive * 60 + exclusive * 40)
        if (rank >= threshold) { combinations.push({ patterns: patternArr, rank }) }
      }
    }

    return combinations.sort((a, b) => {
      var diff = b.rank - a.rank
      if (diff === 0) diff = a.patterns.length - b.patterns.length
      return diff
    })
  }
  getBlacklist (patternArr, options) {
    const { links } = this
    const { threshold } = {
      threshold: 90,
      ...options
    }

    const whitelist = []
    const blacklist = {}

    var i = links.length
    while (i--) {
      if (test(links[i], patternArr)) {
        whitelist.push(links[i])
      }
    }

    i = links.length
    while (i--) {
      if (whitelist.indexOf(links[i]) === -1) {
        for (var segment, x = 0; x < links[i].segments.length; x++) {
          if (!(segment = links[i].segments[x])) continue
          if (blacklist[x] && blacklist[x].indexOf(segment) !== -1) break

          var matching = 0
          var j = whitelist.length
          while (j--) {
            if (whitelist[j].segments[x] !== segment) matching++
          }

          if ((matching / whitelist.length * 100) >= threshold) {
            if (!blacklist[x]) blacklist[x] = [segment]
            else blacklist[x].push(segment)
            break
          }
        }
      }
    }

    return blacklist
  }
  getSearchResults (patternArr, blacklist) {
    const { links } = this
    const results = []

    var i = links.length
    while (i--) {
      if (test(links[i], patternArr)) {
        if (blacklist) {
          var { segments } = links[i]
          var x = segments.length
          var blacklisted = false
          while (x--) {
            if (blacklist[x] && blacklist[x].indexOf(segments[x]) !== -1) {
              blacklisted = true
              break
            }
          }

          if (!blacklisted) {
            results.push(links[i])
          }
        } else {
          results.push(links[i])
        }
      }
    }

    return results
  }
}

function test (link, patternArr) {
  'use strict'
  const { segments } = link

  var i = patternArr.length
  while (i--) {
    var { index, type, value } = patternArr[i]

    if (!segments[index]) return false

    var match
    var isMatch = false

    if (regex[type]) {
      isMatch = regex[type].test(segments[index])
    } else {
      switch (type) {
        case 'pathPrefix':
          isMatch = segments[index].startsWith(value)
          break
        case 'pathSuffix':
          isMatch = segments[index].endsWith(value)
          break
        case 'exactPath':
          isMatch = segments[index] === value
          break
        case 'substring':
          isMatch = segments[index].indexOf(value) !== -1
          break
        case 'trailingHex':
          isMatch = segments[index].match(trailingHex) !== null
          break
        case 'preHexID':
          match = segments[index].match(trailingHex)
          isMatch = match ? match[1] === value : false
          break
        case 'nameSuffix':
          match = segments[index].match(trailingHex)
          isMatch = match
            ? segments[index].slice(0, -match[0].length).endsWith(value) : false
          break
        case 'trailingID':
          match = segments[index].match(trailingID)
          isMatch = match ? match[2].length === +value : false
          break
        case 'length':
          isMatch = segments.length === +value
          break
        case 'escapedSpace':
          isMatch = escapedSpace.test(segments[index])
          break
        case 'normalized':
          if (link.text &&
              link.text.length >= segments[index].length / 2) {
            var norm; var text = link.text.toLowerCase()
            if (text !== segments[index]) {
              text = normalize(link.text)
              norm = normalize(segments[index])

              isMatch = norm.indexOf(text) !== -1 || text.indexOf(norm) !== -1
            }
          }
          break
        default:
          throw new Error('Unexpected pattern type: ' + type)
      }
    }

    if (!isMatch) return false
  }

  return true
}

function getDesc (link, x, isNameIndex, term) {
  'use strict'
  if (!descMap.has(link)) descMap.set(link, {})

  var current = descMap.get(link)

  if (!current[x]) {
    var segment = link.segments[x]
    var desc = current[x] = {}

    if (isNameIndex) {
      desc.trailingHex = segment.match(trailingHex)
      desc.trailingID = segment.match(trailingID)

      var norm; var text = link.text
      if (text && text.indexOf(term) !== -1 &&
          text.length >= segment.length / 2) {
        text = text.toLowerCase()
        if (text !== segment) {
          text = normalize(text)
          norm = normalize(segment)

          if (norm.indexOf(text) !== -1 || text.indexOf(norm) !== -1) {
            desc.normalized = true
          }
        }
      }
    } else {
      var k = types.length
      while (k--) desc[types[k]] = regex[types[k]].test(segment)
    }
  }

  return current[x]
}

function getCommonPrefix (strA, strB) {
  'use strict'
  var i = -1
  var length = strA.length

  while (++i < length) {
    if (strA[i] !== strB[i]) break
  }

  return i > 0 ? strA.slice(0, i) : ''
}

function getCommonSuffix (strA, strB) {
  'use strict'
  const diff = strA.length - strB.length

  if (diff > 0) strA = strA.slice(diff)
  else if (diff < 0) strB = strB.slice(diff)

  var i = strA.length

  while (i--) {
    if (strA[i] !== strB[i]) break
  }

  return i < strA.length - 1 ? strA.slice(-i - 1) : ''
}

function normalize (str) {
  return unescape(str).replace(/[^a-z]+/g, '')
}
