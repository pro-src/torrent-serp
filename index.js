const { LinkParser, fetchLinks } = require('./lib/links')
const SERP = require('./lib/serp')

module.exports = { SERP, LinkParser, fetchLinks }

async function test (uri, term) {
  const links = await fetchLinks(uri)
  const page = new SERP(links, term)

  const patterns = page.getPatterns({ threshold: 90 })
  const combinations = page.getUnigueCombinations(patterns)

  const patternArr = combinations[0].patterns

  const blacklist = page.getBlacklist(patternArr)
  const results = page.getSearchResults(patternArr, blacklist)

  return {
    links,
    page,
    patterns,
    combinations,
    blacklist,
    results
  }
}
