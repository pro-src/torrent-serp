# torrent-serp
Torrent SERP Pattern Detection

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

This is a dependency of a larger project that I've been working on.
It doesn't quite extract the results from any torrent site by itself.
It doesn't account for sites that use the torrent info hash to ID their results or sites that use magnet links.

***The finished project will be capable of searching and returning the results from ANY torrent site.***

I just need to settle on a name, deal with cloudfare and do code cleanup. Star this repo as I'll update it's README to point to the finished project.
